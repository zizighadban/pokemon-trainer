# Application - Pokémon Trainer

# Instructions to open the web application

- Open the folder of the project -> right-click anywhere in the folder -> click 'Git Bash Here'
- Run 'npm install' in Git Bash
- Open code by running 'code .' in Git Bash 
- in Visual Studio Code, open the terminal and run 'ng serve -o'. The project will now open in your web-browser

# Description

- The goal of the web application is for users to be able to catch Pokémons. When the users have caught a Pokémon, the Pokémons are then added to the user's profile in Trainer Page. Users have the option to "Remove a Pokémon" and "Log Out" back to the Landing Page.

# Developers

- Leon Listo
- Zahra Ghadban