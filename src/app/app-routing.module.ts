import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { LandingPage } from "./landing-page/landing-page.page";
import { PokemonsPage } from "./pokemons/pokemons.page";
import { TrainerPage } from "./trainer-page/trainer.page";

const routes: Routes = [
    {
        // Trainer gets redirected to /landing-page even when the path is '/'
        path: '',
        pathMatch: 'full',
        redirectTo: '/landing-page'
    },
    {
        path: 'landing-page',
        component: LandingPage
    },
    {
        path: 'pokemons',
        component: PokemonsPage,
        // This page can only be activated when there's a trainer in the local storage
        canActivate: [AuthGuard]
    },
    {
        path: 'trainer-page',
        component: TrainerPage,
        canActivate: [AuthGuard]   
    }
]

@NgModule({
    // Accept the arguments for the routes
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}