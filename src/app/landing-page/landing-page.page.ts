import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { Trainer } from "../models/trainer.model";
import { TrainerService, TRAINER_KEY } from "../services/trainer.service";

declare let alertify: any;

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.page.html',
    styleUrls: ['./landing-page.page.css'],

})
export class LandingPage implements OnInit {

    // Dependency Injection
    // Inject the services into the landing page
    constructor(
        private router: Router,
        private trainerService: TrainerService
    ) { }

    // If trainer is logged in, they can't redirect to Landing Page until they log out/delete from localStorage 
    ngOnInit(): void {
        if(localStorage.getItem(TRAINER_KEY)){
            const trainerStored: Trainer = JSON.parse(localStorage.getItem(TRAINER_KEY) || '');
            this.trainerService.setTrainer(trainerStored);
            this.router.navigateByUrl('/pokemons');
        }
    }

    onSubmit(form: NgForm): void {
        // Check the trainer
        // Destruct the trainer name from the value property
        const { trainerName } = form.value;
        // Setter
        this.trainerService.trainerName = trainerName;

        // Redirect to Pokemon Catalogue Page
        this.router.navigateByUrl('pokemons');

        // Adds trainer to api
        this.trainerService.createTrainer(trainerName);
    }


}