import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { TrainerService } from "../services/trainer.service";

declare let alertify: any;
@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

    constructor(
        private router: Router,
        private trainerService: TrainerService
    ) { }

    ngOnInit(): void {
        // Call api function
        this.trainerService.apiFetchPokemonsList()
    }

    removePokemon(pokemon: any): void {
        // Remove pokemon api call
        this.trainerService.removePokemon(pokemon);

        alertify.message('Pokémon Removed!')
    }

    // Get trainer name and pokemon list
    get trainerName(): string {
        return this.trainerService.trainerName
    }
    get pokemonFromList(): any {
        return this.trainerService.apiFetchPokemonsList();
    }

    onLogout() {

        this.router.navigateByUrl('/landing-page')
        // Resets the localStorage when the trainer logs out
        localStorage.clear()

        // Message pops out telling trainer that they are now logged out
        alertify.message('You are logged out!')
    }
}