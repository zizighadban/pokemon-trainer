import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../models/trainer.model";
import { SelectedPokemonService } from "../services/selected-pokemon.service";
import { TrainerService } from "../services/trainer.service";

declare let alertify: any;
@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue-page.component.html',
    styleUrls: ['./catalogue-page.component.css']
})
export class PokemonComponent implements OnInit {

    constructor(
        private readonly trainerService: TrainerService,
        private readonly selectedPokemonService: SelectedPokemonService,
    ){}
    
    get pokemon(): Pokemon | null {
        return this.selectedPokemonService.trainer()
    }
    ngOnInit(): void {
        this.trainerService.apiFetchPokemons();
    }

    get trainerName(): string{
        return this.trainerService.trainerName
    }

    get pokemons(): any {
        return this.trainerService.getPokemons();
    }

    //Click function. Pokemon gets caught and added in to users pokemon list in api
    onPokemonClicked(pokemon: Pokemon): void {
        this.selectedPokemonService.setTrainer(pokemon);
        this.trainerService.postPokemon(pokemon);
        let selected = this.selectedPokemonService.setTrainer(pokemon);
        sessionStorage.setItem('collected', JSON.stringify(selected));
        alertify.message('Pokémon Caught!');
    }
}