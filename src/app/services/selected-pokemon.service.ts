import { Injectable } from "@angular/core";
import { Pokemon } from "../models/trainer.model";

@Injectable({
    providedIn: 'root'
})
export class SelectedPokemonService{
    private _pokemon: Pokemon | null = null;

    public setTrainer(pokemon: Pokemon){
        this._pokemon = pokemon;
    }
    public trainer(): Pokemon | null {
        return this._pokemon;
    }
}