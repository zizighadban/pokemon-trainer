import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core"
import { catchError, Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { Trainer } from "../models/trainer.model";
import { Pokemon } from "../models/trainer.model";
import { MessageService } from "./message.service";

export const TRAINER_KEY = 'pokemon-trainerName'
const URL = environment.pokemonAPI;

// Create one instance of the service so when it's used, the same object is shared across all components
@Injectable({
    providedIn: 'root'
})

// Service that can be injected in any file/component in the application
export class TrainerService {


    private _trainerName: string = '';
    private _trainer: Trainer | null = null;
    private _error: string = '';
    private _pokemons: Pokemon[] = [];


    // Getters
    get pokemons(): Pokemon[] {
        return this._pokemons;
    }

    get trainerName(): string {
        return this._trainer?.username || ''
    }

    // Setter
    set trainerName(trainerName: string) {
        // Save the trainer name in local storage
        localStorage.setItem(TRAINER_KEY, trainerName)
        this._trainerName = trainerName;
    }

    public setTrainer(trainer: Trainer): void {
        this._trainer = trainer;
    }

    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) {
        // Prevents the trainer's name to disappear from the Pokemon Catalogue Page when reloading the page  
        this._trainerName = localStorage.getItem(TRAINER_KEY) || '';
    }

    // Create headers for api calls that require them
    private createHeaders(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': 'qwerty'
        })
    }

    // Create a trainer on login
    createTrainer(username: string): void {
        this.http.get<Trainer[]>(`${URL}?username=${username}`)
            .subscribe((trainer: Trainer[]) => {
                // Checks if trainer is already in the API
                if (trainer.length === 0) {
                    const user = {
                        username: username,
                        pokemon: []
                    }
                    const headers = this.createHeaders()
                    // Posts trainer into API
                    this.http.post<Trainer>(`${URL}?username=${username}`, user, { headers })
                        .subscribe({
                            next: (response: Trainer) => {
                                this._trainer = response;
                                localStorage.setItem(TRAINER_KEY, JSON.stringify(response));
                            }
                        })
                }
                else {
                    // If user already exists do not create new one
                    this._trainer = trainer.pop() || null;
                    localStorage.setItem(TRAINER_KEY, JSON.stringify(this._trainer));
                }
                (error: HttpErrorResponse) => {
                    this._error = error.message;
                }
            })
    }

    // Fetches pokemons from the poke api
    apiFetchPokemons(): void {
        this._pokemons = [];
        this.http.get<Pokemon[]>(`https://pokeapi.co/api/v2/pokemon?offset=0&limit=20`)
            .pipe(catchError(this.errorHandling<any>(`fetchPokemons`, [])))
            .subscribe({
                next: (response) => {
                    for (let i = 0; i < 20; i++) {
                        let _id = 0 + i + 1;
                        let _imageURL = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${_id}.png`;
                        let pokemon = {
                            id: _id,
                            name: response.results[i].name,
                            image: _imageURL,
                            collected: false
                        }
                        this.pokemons?.push(pokemon)
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            })
    }

    // Removes pokemon on trainer page
    removePokemon(pokemon: Pokemon): void {
        let getTrainerId = this._trainer?.id;
        const headers = this.createHeaders();
        let getPokemon = this._trainer?.pokemon;

        this.http.patch(`${URL}${getTrainerId}`, { pokemon: this._trainer?.pokemon }, { headers })
            .subscribe((pokemon: any) => {
                getPokemon?.pop();
                localStorage.removeItem(pokemon);
            },
                (error: HttpErrorResponse) => {
                    this._error = error.message;
                });
    }

    // Message for error handling
    private log(message: string) {
        this.messageService.message = `TrainerService: ${message}`
    }

    // Error handling for apiFetchPokemons()
    private errorHandling<T>(procedure = 'procedure', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);

            this.log(`${procedure} errorCaught: ${error.message}`);

            return of(result as T);

        }
    }

    // Patch pokemon function. Pokemon that gets clicked gets added to local storage and database
    postPokemon(pokemon: Pokemon): void {
        let getTrainerId = this._trainer?.id;
        let getPokemon = pokemon.name;
        let pushToArray = this._trainer?.pokemon.push(getPokemon);
        const headers = this.createHeaders()

        this.http.patch<any>(`https://api-lz.herokuapp.com/trainers/${getTrainerId}`, { pokemon: this._trainer?.pokemon }, { headers })
            .subscribe((pokemon: Trainer[]) => {
                console.log(pokemon)
                localStorage.setItem(TRAINER_KEY, JSON.stringify(pokemon))
            })
    }

    // Get the logged in users pokemon collection from api
    getPokemonList(): void {
        let getTrainerId = this._trainer?.id;


        this.http.get<any>(`${URL}${getTrainerId}`)
            .subscribe((pokemon: any) => {
                console.log(pokemon)
            },
                (error: HttpErrorResponse) => {
                    this._error = error.message;
                });
    }

    // Declare functions for use
    public getPokemons(): Pokemon[] {
        return this._pokemons;
    }
    
    public apiFetchPokemonsList(): any {
        return this._trainer?.pokemon
    }

    public error(): string {
        return this._error;
    }
}