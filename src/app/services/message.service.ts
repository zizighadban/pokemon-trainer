import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
//Message service for error handling when fetching from api
export class MessageService{
    constructor(){}

    message: string = ''

    set(message: string){
        this.message = message
    }

    clear(){
        this.message = ''
    }
}