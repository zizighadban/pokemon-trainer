import { Component } from "@angular/core";
import { SelectedPokemonService } from "../services/selected-pokemon.service";

@Component({
    selector: 'app-pokemon-selected',
    templateUrl: './pokemon-selected.component.html',
    styleUrls: ['./pokemon-selected.component.css']
})
export class PokemonSelectedComponent {
    constructor(
        private readonly selectedPokemonService: SelectedPokemonService
    ) {}
    
}

