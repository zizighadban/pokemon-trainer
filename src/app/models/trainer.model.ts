// Specify data types for the properties in the API
export interface Trainer{
    id: number,
    username: string,
    pokemon: string []
}

export interface Pokemon {
    id: number,
    name: string,
    image: string,
    collected: boolean
}

export interface PokemonCollect {
    pokemon: Pokemon,
    id: number,
    catch: boolean
}

export interface PokemonRawData {
    results: Array<Pokemon>
}