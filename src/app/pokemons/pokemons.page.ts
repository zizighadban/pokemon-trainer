import { Component } from "@angular/core";
import { TrainerService } from "../services/trainer.service";

@Component({
    selector: 'app-pokemons',
    templateUrl: './pokemons.page.html',
    styleUrls: ['./pokemons.page.css']
})
export class PokemonsPage {

    get trainerName(): string{
        return this.trainerService.trainerName;
    }

    constructor(
        private trainerService: TrainerService
    ){}
}