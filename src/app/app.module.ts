import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { PokemonComponent } from './catalogue-page/catalogue-page.component';
import { PokemonSelectedComponent } from './pokemon-selected/pokemon-selected.component';
import { AppComponent } from './app.component';

import { LandingPage } from './landing-page/landing-page.page';
import { PokemonsPage } from './pokemons/pokemons.page';
import { TrainerPage } from './trainer-page/trainer.page';

//Decorator
@NgModule({ 
  // Register components. Components goes to declarations
  declarations: [
    AppComponent,
    LandingPage,
    PokemonComponent,
    PokemonSelectedComponent,
    PokemonsPage,
    TrainerPage
  ],
  // Everything that is a module goes to imports
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
